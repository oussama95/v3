<?php
require_once("Product.php");
class Book extends All
{
    public function __construct($SKU, $name, $price, $catId, $weight)
    {
        $this->name = $name;
        $this->SKU = $SKU;
        $this->price = $price;
        $this->catId = $catId;
        $this->property = $weight;
    }
}
?>