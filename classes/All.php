<?php
require_once("Product.php");
class All extends Product{

    public function SaveItem()
    {
        try {
            if($this->CheckSKU())
            {
            $item = R::dispense('item');
            $item->sku = $this->SKU;
            $item->name = $this->name;
            $item->price = $this->price;
            $item->cat_id = $this->catId;
            $item->properties = $this->property;
            R::store($item);
            return true;
            }
            else //if we have dublicate SKU in the database so go back to enter new item
			{
				echo "<script>
							alert('SKU is already found in the database');
							window.location.href='add.php';
					    </script>";
			}
        } catch (\Throwable $th) {
            return false;
        }
    }
    
    public function GetItems()
    {
        $item = R::dispense('item');
        $cat = R::dispense('cat');
        $item = R::findAll('item');
        $cat = R::findAll('cat');
        foreach ($item as $row) {
            $temp = R::load('cat',$cat[$row->cat_id]->id);
            $item = new All;
            $item->SetSKU($row->sku);
            $item->SetName($row->name);
            $item->SetPrice($row->price);
            $item->SetCatId($row->cat_id);
            $item->SetProperty($row->properties);
            $item->SetUnit($temp->unit);
            $item->SetCaption($temp->att);
            $data[] = $item; 
        }
        return $data;
    }

} 
?>