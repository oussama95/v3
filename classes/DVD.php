<?php
require_once("Product.php");
class DVD extends All
{

    public function __construct($SKU, $name, $price, $catId, $size)
    {
        $this->name = $name;
        $this->SKU = $SKU;
        $this->price = $price;
        $this->catId = $catId;
        $this->property = $size;
    }

}
?>