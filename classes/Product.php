<?php
require("Database.php");
abstract class Product
{
    protected $SKU;
    protected $name;
    protected $price;
    protected $catId;
    protected $property;
    protected $unit;
    protected $caption;
    
    // Getters code...........
    public function GetSKU()
    {
        return $this->SKU;
    }
    
    public function Getname()
    {
        return $this->name;
    }
    
    public function GetPrice()
    {
        return $this->price;
    }
    
    public function GetCatId()
    {
        return $this->catId;   
    }

    public function GetUnit()
	{
		return $this->unit;
    }
    
	public function GetCaption()
	{
		return $this->caption;
    }
    public function GetProperty()
    {
        return $this->property;
    }



    // Setters code...........
    protected function SetSKU($SKU)
    {
        $this->SKU = $SKU;
    }
    protected function SetName($name)
    {
        $this->name = $name;
    }
    protected function SetPrice($price)
    {
        $this->price = $price;
    }
    protected function SetCatId($catId)
    {
        $this->catId = $catId;
    }
    protected function SetProperty($property)
    {
        $this->property = $property;
    }
	public function SetUnit($unit)
	{
		$this->unit = $unit;
	}
	public function SetCaption($cap)
	{
		$this->caption = $cap;
	}
    
    // Abstract Functions code...........
    public abstract function SaveItem();
    public abstract function GetItems();


    // Class Function...........
    protected function CheckSKU()
    {

        try {
            $prod = R::getRow("SELECT sku FROM item WHERE sku = :sku",['sku' => $this->SKU]);
            if ($prod != null)
            {
                return false;
			}
			return true;
        } catch (Exception $e) {
			echo "EXEPTION IN CHECK          " . $e->getMessage();
			return false;
        }    
    }
    public function DeleteProducts(array $item)
	{
		$str = "";
		foreach ($item as $row) {
			$str .= "'" . $row . "'";
			if (next($item) == true) {
				$str .= ",";
			}
		}
		R::exec("DELETE FROM item WHERE SKU IN ($str)");
    }
}
?>