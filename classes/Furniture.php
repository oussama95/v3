<?php
require_once("Product.php");
class Furniture extends All
{
    public function __construct($SKU, $name, $price, $catId, $height,$width,$length)
    {
        $this->name = $name;
        $this->SKU = $SKU;
        $this->price = $price;
        $this->catId = $catId;
        $this->property = $height."x".$width."x".$length;
    }
    
}
?>