<?php
require_once("Database.php");
class Category extends Database{
    private $properties;

    public function __set($name, $value)
    {
        $this->properties[$name] = $value;
    }
    
    public function __get($propertyName)
    {
        if(array_key_exists($propertyName, $this->properties))
        {
            return $this->properties[$propertyName];
        }
    }
    public function GetCat()
    {
        $result = R::dispense('cat');
        $result = R::findAll('cat');
        if($result->rowCount() > 0)
        {
            while ($row = $result->fetch()) {
                $c = new Category;
                $c->id = $row['id'];
                $c->name = $row['name'];
                $c->att = $row['att'];
                $c->unit = $row['unit'];
                $data[] = $c;
            }
            return $data;
        }
    }
    public function FillSelect()
    {
        $res = R::dispense('cat');
        $res = R::findAll('cat');
        foreach($res as $row) 
        {
            $id = $row['id'];
            $name = $row['name'];
            echo "<option value='$id'>$name</option>";
        }
    }
}
?>