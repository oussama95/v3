<?php
require_once("classes/All.php");
require_once("classes/Book.php");
require_once("classes/DVD.php");
require_once("classes/Furniture.php");

    if(isset($_POST['select'])){ // When coming from index
        $select = $_POST['select']; //selected option

        if($select == 1)
        {   
            if(isset($_POST['sku']))
            {
                    $checked = $_POST['sku']; //array of selected items
                    $item = new All;
                    $item->DeleteProducts($checked);
                    echo "<script>
                            alert('Done!');
                            window.location.href='index.php';
                            </script>";
            }else
            {
                echo "<script>
                alert('No selected Items to delete');
                window.location.href='index.php';
                </script>";
            }
        }else if($select == 2)
        {
            header("Location:add.php");
        }
    }


    if(isset($_POST['add-submit']))// if coming from add.php
    {
            $name = $_POST['name'];
            $sku = $_POST['sku'];
            $price = $_POST['price'];
            $catId = $_POST['type'];
            $prop = "";
            $item;
            if( $catId == 1)
            {   
                $prop = $_POST['size'];
                $item = new DVD($sku, $name, $price,$catId,$prop);
            }
            elseif ($catId == 2) 
            {
                $prop = $_POST['weight'];
                $item = new Book($sku, $name, $price,$catId,$prop);
            }
            elseif ($catId == 3)
            {   
                $h = $_POST['height'] ; 
                $w = $_POST['width'] ;
                $l = $_POST['length'];
                $item = new Furniture($sku, $name, $price,$catId,$h,$w,$h);
            }
            $item->SaveItem();// Saving the Item
            echo "<script>
                alert('Done');
                window.location.href='index.php';
                </script>";
    }

?>